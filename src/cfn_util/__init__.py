import datetime
import logging
import threading
import time
from json import dumps as dicttojson

import boto3
from dateutil.tz import tzutc


EVENT_PRINT_FORMAT = '[{timestamp}] - [{stack_name}]\t[{logical_resource_id}:{resource_status}] - {resource_status_reason}'


# these functions exist so that boto3 can be mocked/patched out
# for tests
def _aws_client(*args, **kwargs):
    return boto3.client(*args, **kwargs)


def _aws_resource(*args, **kwargs):
    return boto3.resource(*args, **kwargs)


def _list_events(stack_name, as_string=False):
    """
    List events for an already deployed stack '``stack_name``'. Wraps `CloudFormation describe_stack_events <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.describe_stack_events>`_

    An 'event' here is an action that's taken place due to a CloudFormation
    deploy, update, or delete.

    :param stack_name: the name of the stack to list events for
    :param as_string: if set, return the list as a JSON string
    :return: a list or JSON string of stack events
    """
    stack_events = _aws_client('cloudformation').describe_stack_events(
        StackName=stack_name
    )
    if as_string:
        return dicttojson(stack_events, indent=2, skipkeys=True, default=repr)
    return stack_events


def _format_list_stack_events(event):
    """
    Format an event from `list_stack_events`. Override ``EVENT_PRINT_FORMAT`` to change
    how events are printed. The following keys are valid:

    * timestamp
    * stack_name
    * logical_resource_id
    * resource_status
    * resource_status_reason

    One could simply override this method as well, but keep in mind the event comes from
    CloudFormation. This method uses the following keys from CFN events:

    * Timestamp
    * StackName
    * LogicalResourceId
    * ResourceStatus
    * ResourceStatusReason (if available)

    :param event: the event to format
    :return: a string representing the ``event`` formatted based on ``EVENT_PRINT_FORMAT``
    """
    return EVENT_PRINT_FORMAT.format(
        timestamp=event['Timestamp'].strftime('%m-%d-%Y %H:%M:%S'),
        stack_name=event['StackName'],
        logical_resource_id=event['LogicalResourceId'],
        resource_status=event['ResourceStatus'],
        resource_status_reason=event.get('ResourceStatusReason', ''),  # status_reason is effectively optional
    )


def parameter(name, value=None, use_previous_value=None):
    """
    Return the Parameter spec that `CloudFormation uses <https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_Parameter.html>`_

    :param name: the name of the parameter; the ParameterKey
    :param value: the value of the parameter; the ParameterValue. If not specified, ``use_previous_value`` must be True
    :param use_previous_value: whether or not to use the previous value of the parameter. Only valid for updates.
        If this is not specified, ``value`` must be
    :return: dict of `CloudFormation Parameter Spec
        <https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_Parameter.html>`_
    """
    param = dict(
        ParameterKey=name,
    )
    if use_previous_value:
        param.update(UsePreviousValue=True)
    elif value:
        param.update(ParameterValue=value)
    else:
        raise KeyError('Either value or use_previous_value must be specified')
    return param


def describe_stack(stack_name, as_string=False):
    """
    Describe the stack named ``stack_name``. A stack ID is also valid here. Wraps
    `CloudFormation describe_stacks <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.describe_stacks>_`

    :param stack_name: the stack to describe
    :param as_string: if set, return the list as a JSON string
    :return: a JSON description of the stack
    """
    stack_description = _aws_client('cloudformation').describe_stacks(
        StackName=stack_name
    )['Stacks'][0]
    if as_string:
        return dicttojson(stack_description, indent=2, skipkeys=True, default=repr)
    return stack_description


def describe_stack_resources(stack_name, as_string=False):
    """
    Describe the resources of the stack named ``stack_name``. A stack ID is also valid here. Wraps
    `CloudFormation describe_stack_resources <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.describe_stack_resources>_`

    :param stack_name: the stack to describe
    :param as_string: if set, return the list as a JSON string
    :return: a JSON description of the stack
    """
    stack_description = _aws_client('cloudformation').describe_stack_resources(
        StackName=stack_name
    )['StackResources']
    if as_string:
        return dicttojson(stack_description, indent=2, skipkeys=True, default=repr)
    return stack_description


def deploy(stack_name, body: str, capabilities=None, **params):
    """
    Deploy a CFN stack

    :param stack_name: the name of the stack
    :param body: the body of the template as a string
    :param capabilities: a string array representing the stack's capabilities; valid values

        * CAPABILITY_IAM
        * CAPABILITY_NAMED_IAM

    :param params: the parameters for the stack, provided as ParameterKey=ParameterValue
    :return: a CreateStack response
    """
    args = dict(
        StackName=stack_name,
        TemplateBody=body,
        # RoleARN=None,
        OnFailure='DELETE',
        TimeoutInMinutes=20,
    )
    if capabilities:
        args.update(Capabilities=capabilities)
    if params:
        args['Parameters'] = [parameter(k, v) for k, v in params.items()]
    return _aws_client('cloudformation').create_stack(**args)


def update(stack_name, body, capabilities=None, **params):
    """
    Update a CFN stack

    :param stack_name: the name of the stack
    :param body: the body of the template as a string
    :param capabilities: a string array representing the stack's capabilities; valid values

        * CAPABILITY_IAM
        * CAPABILITY_NAMED_IAM

    :param params: the parameters for the stack, provided as ParameterKey=ParameterValue
    :return: a CreateStack response
    """
    args = dict(
        StackName=stack_name,
        TemplateBody=body,
    )
    cloudformation = _aws_resource('cloudformation')
    stack = cloudformation.Stack(stack_name)
    if capabilities:
        args.update(Capabilities=capabilities)
    else:
        current_capabilities = stack.capabilities
        if current_capabilities:
            args.update(Capabilities=current_capabilities)
    if params:
        args['Parameters'] = [parameter(k, v) for k, v in params.items()]
    else:
        parameters = stack.parameters
        args['Parameters'] = [parameter(p['ParameterKey'], use_previous_value=True) for p in parameters]
    return _aws_client('cloudformation').update_stack(**args)


def delete(stack_name):
    """
    A simple wrapper for `CloudFormation.Client.delete_stack <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.delete_stack>`_

    :param stack_name: the name of the stack to delete
    :return: None
    """
    return _aws_client('cloudformation').delete_stack(StackName=stack_name)


def validate(body):
    """
    A simple wrapper for `cloudformation.validate_template <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.validate_template>`_

    :param body: the template to validate as a string
    :return: a dict, or else an exception is raised
    """
    return _aws_client('cloudformation').validate_template(TemplateBody=body)


def wait_for_deploy_complete(stack_name):
    """
    Creates a thread to wait for a ``stack_name`` to be deployed

    :param stack_name: the stack to wait for
    :return: a Thread whose target is `Cloudformation.Waiter.stack_create_complete <https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Waiter.StackCreateComplete>`_
    """
    waiter = _aws_client('cloudformation').get_waiter('stack_create_complete')
    return threading.Thread(name='create stack waiter', target=waiter.wait, kwargs=dict(StackName=stack_name))


def wait_for_update_complete(stack_name):
    waiter = _aws_client('cloudformation').get_waiter('stack_update_complete')
    return threading.Thread(name='update stack waiter', target=waiter.wait, kwargs=dict(StackName=stack_name))


def wait_for_delete_complete(stack_name):
    waiter = _aws_client('cloudformation').get_waiter('stack_delete_complete')
    return threading.Thread(name='delete stack waiter', target=waiter.wait, kwargs=dict(StackName=stack_name))


# TODO create wrapper for waiter.wait so that we can catch the exception thrown
# TODO improve how this works:
#   halt early on "Rollback Complete" and "Update Complete (for stack resources)"
#   
def wait_for_cloudformation(stack_name, _waiter: threading.Thread, log=True):
    def event_loop():
        delay = 5
        while _waiter.is_alive():
            if log:
                try:
                    time_boundary = datetime.datetime.now(tz=tzutc()) - datetime.timedelta(seconds=delay)
                    for e in reversed(_list_events(stack_name)['StackEvents']):
                        if e['Timestamp'] > time_boundary:
                            print(_format_list_stack_events(e))
                except:
                    try:
                        print(_list_events(stack_name))
                    except Exception as e:
                        logging.error('Fatal exception in event spool; halting.')
                        logging.exception(e)
                        return
            time.sleep(delay)

    _waiter.start()
    event_spool = threading.Thread(name='cfn_util event spool', target=event_loop)
    event_spool.start()
    _waiter.join()
    event_spool.join()


def deploy_and_wait(stack_name, body, capabilities=None, **params):
    """
    A convenience function that calls :func:`deploy <cfn_util.deploy>` and :func:`wait_for_create_complete
    <cfn_util.wait_for_deploy_complete>`

    :param stack_name: the name of the stack
    :param body: the body of the template as a string
    :param capabilities: a string array representing the stack's capabilities; valid values:

        * CAPABILITY_IAM
        * CAPABILITY_NAMED_IAM

    :param params: the parameters for the stack, provided as ParameterKey=ParameterValue
    :return: a CreateStack response TODO: link
    """
    deploy(stack_name, body, capabilities=capabilities, **params)
    wait_for_cloudformation(stack_name, wait_for_deploy_complete(stack_name))


def update_and_wait(stack_name, body, capabilities=None, **params):
    """
    A convenience function for :func:`update <cfn_util.update>` and :func:`wait_for_update_complete
    <cfn_util.wait_for_update_complete>`

    :param stack_name: the name of the stack
    :param body: the body of the template as a string
    :param capabilities: a string array representing the stack's capabilities; valid values:

        * CAPABILITY_IAM
        * CAPABILITY_NAMED_IAM

    :param params: the parameters for the stack, provided as ParameterKey=ParameterValue
    :return: a UpdateStack response TODO: link
    """
    update(stack_name, body, capabilities=capabilities, **params)
    wait_for_cloudformation(stack_name, wait_for_update_complete(stack_name))


def delete_and_wait(stack_name):
    """
    A convenience function for :func:`delete <cfn_util.delete>` and :func:`wait_for_delete_complete
    <cfn_util.wait_for_delete_complete>`

    :param stack_name: the name of the stack
    :return: a DeleteStack response TODO: link
    """
    delete(stack_name)
    wait_for_cloudformation(stack_name, wait_for_delete_complete(stack_name))