import logging

from cfn_util import (
    parse_arguments,
    wait_for_cloudformation,
    wait_for_deploy_complete,
    deploy,
    update,
    delete,
    wait_for_update_complete,
    wait_for_delete_complete,
)


# TODO: refactor. main should take in arguments instead of creating them.
#   I also don't like that this is the only place in the 'library' that
#   actually does any waiting, despite the util methods prefixed with 'wait_for'
#   'main' should just parse CLI parameters and pass them directly to a handler
#   in cfn_util
# TODO: consider click for the cli
def main():
    """
    The cfn_util command line; see ``cfn_util --help`` for more.
    """
    args = parse_arguments.parse()

    stack_name = args.stack_name
    capabilities = args.capabilities
    if args.template_body:
        template_body = args.template_body.read()
    elif args.deploy or args.update:
        raise ValueError('--template-body is required for this operation')
    else:
        template_body = None  # delete action
    parameters: dict = args.stack_parameters
    try:
        if args.deploy:
            deploy(
                stack_name,
                template_body,
                capabilities,
                **parameters,
            )

            wait_for_cloudformation(stack_name, wait_for_deploy_complete(stack_name))
        elif args.update:
            update(
                stack_name,
                template_body,
                capabilities,
                **parameters,
            )

            wait_for_cloudformation(stack_name, wait_for_update_complete(stack_name))
        elif args.delete:
            delete(stack_name)
            wait_for_cloudformation(stack_name, wait_for_delete_complete(stack_name))
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    main()
