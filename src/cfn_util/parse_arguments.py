import argparse
import logging


class StackParameterAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None, *args, **kwargs):
        try:
            parameter, value = values.split('=')
            if not namespace.get('parameters'):
                namespace.__setattr__('parameters', dict())
            namespace.__getattribute__('parameters').update(parameter=value)
        except:
            logging.error('Failed to parse argument; expected `-p <key>=<value>`')
            raise


def setup():
    parser = argparse.ArgumentParser(
        'cfn_util',
        description=
        '''
        CLI for CloudFormation
        '''
    )
    parser.add_argument('--stack-name', '-s', '-n', required=True)
    parser.add_argument('--template-body', '-t', type=argparse.FileType())
    parser.add_argument('--capabilities', '-c', nargs='?', action='append')
    parser.add_argument('--parameter', '-p', dest='parameters', metavar='KEY=VALUE', nargs='?', action='append')

    stack_actions = parser.add_mutually_exclusive_group(required=True)
    stack_actions.add_argument('--deploy', action='store_true')
    stack_actions.add_argument('--update', action='store_true')
    stack_actions.add_argument('--delete', action='store_true')

    return parser


def parse(*args):
    namespace = setup().parse_args(*args)
    if hasattr(namespace, 'parameters') and namespace.parameters:
        namespace.__setattr__('stack_parameters', {x.split('=')[0]: x.split('=')[1] for x in namespace.parameters})
    else:
        namespace.__setattr__('stack_parameters', dict())
    print(namespace)
    return namespace
