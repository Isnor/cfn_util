.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :hidden:

   documentation

.. toctree::
   :caption: Library
   :maxdepth: 2
   :hidden:

   apidoc