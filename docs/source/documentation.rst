*************
Documentation
*************

``cfn_util`` exists because ``aws cloudformation`` is extremely basic and unfriendly. It's extremely verbose,
supplying parameters is just short of a nightmare, and it doesn't give any indication of progress. In
contrast, ``cfn_util`` strives to be an intuitive and informative command line for interacting with CloudFormation.

Commands
=========

Currently, the command line supports the following operations:

	* deploy
	* update
	* delete

Each of them creates a process that waits for the action to complete, printing out updates whenever something
happens with the stack.

