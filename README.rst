########
cfn_util
########

A python library and command line utility for interacting with CloudFormation. See the full documentation
at https://isnor.gitlab.io/cfn_util

Goal
====

cloudformation_util is a python library and command line utility for interacting with CloudFormation. It exists because
I wanted a better interface for interacting with CloudFormation stacks than the `boto3 <https://boto3.amazonaws.com/v1/
documentation/api/latest/reference/services/index.html>`_ library, and I was unsatisfied with the ``aws cloudformation``
command line when it came to managing stacks.

The goal of the project is to provide a sane command line and Python library for managing CloudFormation stacks


Alternatives
============

`aws-cdk <https://docs.aws.amazon.com/cdk/latest/guide/home.html>`_ is far better and more interesting than this if you have
the flexibility and are unencumbered by corporate cost-saving and resource-protection restrictions. In my experience, it
has been useful to be able to interact with "raw cloudformation", but I hated the command line that AWS provided in
``aws cloudformation create-stack``

`aws-sam-cli <https://github.com/awslabs/aws-sam-cli>`_ is a command line tool for managing stacks that use the
serverless application model (SAM). If you're deploying an API gateway, you may want to use SAM instead.


Installation
============

Install with ``pip``:

``pip install cfn_util``


Usage
=====

The CLI is meant to be a more user-friendly and intuitive version of the original ``aws cloudformation``.
Currently, it supports::

	--deploy
	--update
	--delete

CLI::

	cfn_util --deploy -t <template_location> -n <stack_name>

Will start a process that periodically prints out available updates on the stack's creation. If the stack
has a parameter "Foo" and creates an IAM role::

	cfn_util --deploy -p Foo=Bar -c CAPABILITY_IAM -t <template> ...

The same interface is supported by ``--update``. Additionally, update assumes parameters not supplied takes
on their previous value.