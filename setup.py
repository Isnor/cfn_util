from setuptools import setup
from setuptools import find_packages
from os.path import basename
from os.path import splitext
from glob import glob


with open('README.rst') as f:
    long_description = f.read()


setup(
    name='cfn_util',
    version='0.0.5',
    description='',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://gitlab.com/Isnor/cfn_util',
    py_modules='cfn_util',
    install_requires=[
        'boto3',
        'argparse',
    ],
    tests_require=[
        'localstack',
        'awscli-local',
        'pytest',
    ],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'cfn_util = cfn_util.__main__:main',
        ]
    },
    setup_requires=[
        'pytest-runner',
    ],
    classifiers=[
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.7",
            "Operating System :: OS Independent",
        ],
    python_requires='>=3.7',
)
