import logging
import os
import time
import docker
import localstack_client.session as aws
import pytest
import troposphere
from botocore.exceptions import ClientError
from localstack.services import infra as infrastructure
import localstack.utils.bootstrap
from troposphere.s3 import Bucket

import cfn_util
from cfn_util import (
    deploy_and_wait,
    update_and_wait,
    delete_and_wait,
)


def service_started(service, port=None, timeout=None):
    if timeout < 0:
        raise TimeoutError(f'timed out waiting for service "{service}"')
    else:
        if infrastructure.get_service_status(service, port) == 'running':
            logging.info(f'service {service} was started')
            return True
        time.sleep(1)
        logging.debug(f'starting service {service}...')
        return service_started(service, port, timeout-1)


def _aws_client(*args, **kwargs):
    return aws.client(*args, **kwargs)


def _aws_resource(*args, **kwargs):
    return aws.resource(*args, **kwargs)


cfn_util._aws_client = _aws_client
cfn_util._aws_resource = _aws_resource


# logging.basicConfig(level=getattr(logging, os.getenv('PYTHON_LOG_LEVEL', 'INFO').upper()))
logging.basicConfig(level=logging.INFO)
logging.debug('set log level to INFO')


class MockCfnStack(troposphere.Template):
    """
    Extend Troposphere's Template with a name; mocks a stack for testing
    """
    def __init__(self, name):
        super().__init__()
        self.name = name


@pytest.fixture()
def local_aws_environment():
    """
    start/stop localstack
    """
    logging.info('starting AWS services...')
    aws_apis = ['cloudformation', 's3']

    # localstack env. variables
    os.environ['SERVICES'] = ','.join(aws_apis)
    os.environ['START_WEB'] = '0'
    os.environ['FORCE_NONINTERACTIVE'] = str(True)

    process = localstack.utils.bootstrap.run('localstack start', asynchronous=True)
    [service_started(service, timeout=15) for service in aws_apis]
    logging.info('AWS services started.')
    yield process
    logging.info('stopping AWS services...')
    docker_client = docker.from_env()
    docker_client.containers.get('localstack_main').stop(timeout=5)
    process.terminate()
    process.kill()
    logging.info('AWS services stopped')


@pytest.fixture()
def deploy_test_stack(local_aws_environment):
    test_stack = MockCfnStack('test-stack')
    test_stack.add_resource(Bucket('bucket', BucketName='test-bucket'))

    logging.debug('deploying stack')
    deploy_and_wait(test_stack.name, body=test_stack.to_yaml())
    logging.debug('stack deployed')
    return test_stack


@pytest.fixture()
def update_test_stack(deploy_test_stack):
    deploy_test_stack.add_resource(Bucket('BucketTwo', BucketName='test-bucket-2'))
    logging.debug(f'updating stack {deploy_test_stack.to_yaml(clean_up=True)}')

    update_and_wait(stack_name=deploy_test_stack.name, body=deploy_test_stack.to_yaml())
    logging.debug('updated stack')
    return deploy_test_stack


def stack_exists(name):
    cf = aws.client('cloudformation')
    try:
        cf.describe_stacks(StackName=name)
        return True
    except:
        logging.debug(f'stack "{name}" did not exist')
        return False


def bucket_exists(name):
    s3 = aws.client('s3')
    try:
        s3.head_bucket(Bucket=name)
        logging.info(f'bucket {name} exists')
        return True
    except ClientError:
        logging.info(f'bucket {name} does not exist')
        return False


def test_deploy(deploy_test_stack: MockCfnStack):
    assert stack_exists(deploy_test_stack.name)
    assert bucket_exists('test-bucket')


def test_update(update_test_stack: MockCfnStack):
    # update test stack adds a second bucket, "test-bucket-2"
    assert stack_exists(update_test_stack.name)
    assert bucket_exists('test-bucket')
    assert bucket_exists('test-bucket-2')


def test_delete_stack(deploy_test_stack: MockCfnStack):
    assert stack_exists(deploy_test_stack.name)
    assert bucket_exists('test-bucket')
    logging.info('deleting stack')
    delete_and_wait(deploy_test_stack.name)
    logging.info('stack deleted')
    assert not stack_exists(deploy_test_stack.name)
    assert not bucket_exists('test-bucket')
